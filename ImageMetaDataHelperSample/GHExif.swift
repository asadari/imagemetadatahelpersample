//
//  GHExif.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
class GHExif:GHReflectable {
    var isEmpty : Bool = true
    var apertureValue : Double?
    var brightnessValue : Double?
    var colorSpace : Int?
    var dateTimeDigitized : String?
    var dateTimeOriginal : String?
    var exifVersion : [AnyObject]?
    var exposureBiasValue : Int?
    var exposureMode : Int?
    var exposureProgram : Int?
    var exposureTime : Double? {
        didSet {
            if let exposureTime = exposureTime {
                let speed = 1/exposureTime
                self.shutterSpeed = "1/\(Int(speed))"
            }
        }
    }
    var fNumber : Double?
    var flash : Int?
    var focalLenIn35mmFilm : Double?
    var focalLength : Double?
    var iosSpeedRatings : [AnyObject]?
    var iosSpeedRating : Double? {
        get {
            if let iosSpeedRatings = self.iosSpeedRatings {
                if iosSpeedRatings.count > 0 {
                    if let temp_speed = iosSpeedRatings[0] as? Double {
                        return temp_speed
                    }
                }
            }
            return nil
        }
    }
    var imageUniqueID : String?
    var lensModel : String?
    var lensMake : String?
    var lensSpecification : [AnyObject]?
    var pixelXDimension : Int?
    var pixelYDimension : Int?
    var sceneCaptureType : Int?
    var sceneType : Int?
    var sensingMethod : Int?
    var shutterSpeedValue : Double?
    var whiteBalance : Int?
    var shutterSpeed : String?
    var pixel : Int? {
        get {
            if let pixelXDimension = self.pixelXDimension , let pixelYDimension = self.pixelYDimension {
                return pixelXDimension * pixelYDimension
            }
            return nil
        }
    }
    
    init () {
    }
    
    static func getElementWithData(data : Dictionary<String,AnyObject>?) -> GHExif {
        let exif = GHExif()
        
        if let data = data{
            exif.isEmpty = false
            exif.apertureValue = data[String(kCGImagePropertyExifApertureValue)] as? Double
            exif.brightnessValue = data[String(kCGImagePropertyExifLensModel)] as? Double
            exif.colorSpace = data[String(kCGImagePropertyExifColorSpace)] as? Int
            exif.dateTimeOriginal = data[String(kCGImagePropertyExifDateTimeOriginal)] as? String
            exif.dateTimeDigitized = data[String(kCGImagePropertyExifDateTimeDigitized)] as? String
            exif.exifVersion = data[String(kCGImagePropertyExifVersion)] as? [AnyObject]
            exif.exposureBiasValue = data[String(kCGImagePropertyExifExposureBiasValue)] as? Int
            exif.exposureMode = data[String(kCGImagePropertyExifExposureMode)] as? Int
            exif.exposureProgram = data[String(kCGImagePropertyExifExposureProgram)] as? Int
            exif.exposureTime = data[String(kCGImagePropertyExifExposureTime)] as? Double
            exif.fNumber = data[String(kCGImagePropertyExifFNumber)] as? Double
            exif.flash = data[String(kCGImagePropertyExifFlash)] as? Int
            exif.focalLenIn35mmFilm = data[String(kCGImagePropertyExifFocalLenIn35mmFilm)] as? Double
            exif.focalLength = data[String(kCGImagePropertyExifFocalLength)] as? Double
            exif.iosSpeedRatings = data[String(kCGImagePropertyExifISOSpeedRatings)] as? [AnyObject]
            exif.imageUniqueID = data[String(kCGImagePropertyExifImageUniqueID)] as? String
            exif.lensModel = data[String(kCGImagePropertyExifLensModel)] as? String
            exif.lensMake = data[String(kCGImagePropertyExifLensMake)] as? String
            exif.lensSpecification = data[String(kCGImagePropertyExifLensSpecification)] as? [AnyObject]
            exif.pixelXDimension = data[String(kCGImagePropertyExifPixelXDimension)] as? Int
            exif.pixelYDimension = data[String(kCGImagePropertyExifPixelYDimension)] as? Int
            exif.sceneCaptureType = data[String(kCGImagePropertyExifSceneCaptureType)] as? Int
            exif.sceneType = data[String(kCGImagePropertyExifSceneType)] as? Int
            exif.sensingMethod = data[String(kCGImagePropertyExifSensingMethod)] as? Int
            exif.shutterSpeedValue = data[String(kCGImagePropertyExifShutterSpeedValue)] as? Double
            exif.whiteBalance = data[String(kCGImagePropertyExifWhiteBalance)] as? Int
        }
        return exif
    }
}
