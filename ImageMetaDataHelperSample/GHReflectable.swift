//
//  GHReflectable.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit

protocol GHReflectable {
    func getPropertyName()->[String]
    func getPropertyValue()->[Dictionary<String,AnyObject>]
    func canUsePropertyValue()->[Dictionary<String,AnyObject>]
}

extension GHReflectable {
    func getPropertyName()->[String]{
        var s = [String]()
        for c in Mirror(reflecting: self).children
        {
            if let name = c.label{
                s.append(name)
            }
        }
        return s
    }
    
    func getPropertyValue()->[Dictionary<String,AnyObject>] {
        var s = [Dictionary<String,AnyObject>]()
        for c in Mirror(reflecting: self).children
        {
            if let value = self.unwrap(c.value) as? AnyObject {
                var dic = [String:AnyObject]()
                dic["name"] = c.label
                dic["value"] = value
                s.append(dic)
            }
        }
        return s
    }
    
    func canUsePropertyValue()->[Dictionary<String,AnyObject>] {
        var s = [Dictionary<String,AnyObject>]()
        for c in Mirror(reflecting: self).children
        {
            if let value = self.unwrap(c.value) as? AnyObject {
                if value !== NSNull() {
                    var dic = [String:AnyObject]()
                    dic["name"] = c.label
                    dic["value"] = value
                    s.append(dic)
                }
            }
        }
        return s
    }
    
    func unwrap(any:Any) -> Any {
        let mi = Mirror(reflecting: any)
        if mi.displayStyle != .Optional {
            return any
        }
        if mi.children.count == 0 { return NSNull() }
        let (_, some) = mi.children.first!
        return some
    }
}
