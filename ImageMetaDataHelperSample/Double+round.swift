//
//  Double.swift
//  NotiCopyBySwift
//
//  Created by Gwanho on 2016. 9. 1..
//  Copyright © 2016년 gwanho. All rights reserved.
//

import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}
