//
//  GHTiff.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
class GHTiff:GHReflectable {
    var isEmpty : Bool = true
    var make : String?
    var model : String?
    var orientation : Int?
    var software : String?
    var xResolution : Int?
    var yResolution : Int?
    
    init () {
    }
    
    static func getElementWithData(data : Dictionary<String,AnyObject>?) -> GHTiff {
        let tiff = GHTiff()
        
        if let data = data {
            tiff.isEmpty = false
            tiff.make = data[String(kCGImagePropertyTIFFMake)] as? String
            tiff.model = data[String(kCGImagePropertyTIFFModel)] as? String
            tiff.orientation = data[String(kCGImagePropertyTIFFOrientation)] as? Int
            tiff.xResolution = data[String(kCGImagePropertyTIFFXResolution)] as? Int
            tiff.yResolution = data[String(kCGImagePropertyTIFFYResolution)] as? Int
            tiff.software = data[String(kCGImagePropertyTIFFSoftware)] as? String
        }
        return tiff
    }
}
