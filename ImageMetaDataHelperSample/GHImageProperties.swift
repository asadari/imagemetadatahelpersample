//
//  GHImageProperties.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
class GHImageProperties:GHReflectable {
    var isEmpty : Bool = true
    var dpiHeight : Int?
    var dpiWidth : Int?
    var orientation : Int?
    var pixelHeight : Int?
    var pixelWidth : Int?
    var profileName : String?
    
    init () {
    }
    
    static func getElementWithData(data : Dictionary<String,AnyObject>?) -> GHImageProperties {
        let properties = GHImageProperties()
        if let data = data{
            properties.isEmpty = false
            properties.dpiHeight = data[String(kCGImagePropertyDPIHeight)] as? Int
            properties.dpiWidth = data[String(kCGImagePropertyDPIWidth)] as? Int
            properties.orientation = data[String(kCGImagePropertyOrientation)] as? Int
            properties.pixelHeight = data[String(kCGImagePropertyPixelHeight)] as? Int
            properties.pixelWidth = data[String(kCGImagePropertyPixelWidth)] as? Int
            properties.profileName = data[String(kCGImagePropertyProfileName)] as? String
        }
        return properties
    }
}
