//
//  DetailViewController.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 6..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    var objects = [AnyObject]()
    var defalutDataArray = [AnyObject]()
    var canUseDataArray = [AnyObject]()
    var allDataArray = [AnyObject]()
    @IBOutlet var tableView : UITableView?
    @IBOutlet var contentsViewHeight : NSLayoutConstraint?
    @IBOutlet var contentsView : UIView?
    @IBOutlet var imageView : UIImageView?
    @IBOutlet var segment : UISegmentedControl?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSetting()
        self.objects = self.defalutDataArray
        self.tableView?.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    class func detailViewControllerFromStoryboard() -> DetailViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewControllerWithIdentifier("DetailViewController")
        return viewController as! DetailViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dataSetting() {
        let path = NSBundle.mainBundle().pathForResource("iphonetest", ofType: "jpg")
        let pathUrl = NSURL(fileURLWithPath: path!)
        let image = UIImage(contentsOfFile: path!)
        if let image = image {
            self.imageView?.image = image
        }
        self.defaultDataSetting(pathUrl)
        self.canUseDataSetting(pathUrl)
        self.allDataSetting(pathUrl)
    }
    
    func createObject(name : String, value : AnyObject) -> Dictionary<String,AnyObject> {
        var object = Dictionary<String, AnyObject>()
        object["name"] = name
        object["value"] = value
        return object
    }
    
    //각각의 메타데이터 정보를 조합해서 보여줄 수 있다.
    //
    func defaultDataSetting(pathUrl : NSURL) {
        let meta = GHImageMetaDataHelper.getMetadata(pathUrl)
        if let meta = meta {
            
            //정보가 비어 있으면 넘김
            if meta.tiff.isEmpty == false {
                var dataArray = [Dictionary<String,AnyObject>]()
                if let make = meta.tiff.make {
                    let object = self.createObject("make", value: String(make))
                    dataArray.append(object)
                }
                if let model = meta.tiff.model {
                    let object = self.createObject("model", value: String(model))
                    dataArray.append(object)
                }
                if let software = meta.tiff.software {
                    let object = self.createObject("software", value: String(software))
                    dataArray.append(object)
                }
                if let orientation = meta.tiff.orientation {
                    let value = "\(GHMetaUtils.getStatusStringByOrientationValue(orientation))"
                    let object = self.createObject("orientation", value: String(value))
                    dataArray.append(object)
                    
                }
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray
                    info["title"] = "TIFF"
                    self.defalutDataArray.append(info)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.exif.isEmpty == false {
                var dataArray = [Dictionary<String,AnyObject>]()
                if let dateTimeOriginal = meta.exif.dateTimeOriginal {
                    let object = self.createObject("dateTimeOriginal", value: String(dateTimeOriginal))
                    dataArray.append(object)
                }
                
                if let pixel = meta.exif.pixel {
                    var value = ""
                    var pixel = Double(pixel) / 1000000
                    pixel = pixel.roundToPlaces(1)
                    value += "\(pixel)백만 화소"
                    
                    if let pixelXDimension = meta.exif.pixelXDimension, let pixelYDimension = meta.exif.pixelYDimension {
                        value += "   \(pixelXDimension) * \(pixelYDimension)"
                    }
                    let object = self.createObject("pixcel", value: String(value))
                    dataArray.append(object)
                }
                
                if meta.exif.fNumber != nil || meta.exif.focalLength != nil || meta.exif.iosSpeedRatings != nil {
                    if let fNumber = meta.exif.fNumber {
                        let object = self.createObject("fNumber", value: String(fNumber))
                        dataArray.append(object)
                    }
                    if let focalLength = meta.exif.focalLength {
                        let object = self.createObject("focalLength", value: String(focalLength))
                        dataArray.append(object)
                    }
                    if let shutterSpeed = meta.exif.shutterSpeed {
                        let object = self.createObject("shutterSpeed", value: String(shutterSpeed))
                        dataArray.append(object)
                    }
                    if let iosSpeedRating = meta.exif.iosSpeedRating {
                        let object = self.createObject("iosSpeedRating", value: String(iosSpeedRating))
                        dataArray.append(object)
                    }
                }
                
                if let flash = meta.exif.flash {
                    let value = GHMetaUtils.getStatusStringByFlashValue(flash)
                    let object = self.createObject("flash", value: String(value))
                    dataArray.append(object)
                }
                
                if let lensModel = meta.exif.lensModel {
                    var value = lensModel
                    if let lensMake = meta.exif.lensMake {
                        value += lensMake
                    }
                    let object = self.createObject("Lens Info", value: String(value))
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray
                    info["title"] = "EXIF"
                    self.defalutDataArray.append(info)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.gps.isEmpty == false {
                var dataArray = [Dictionary<String,AnyObject>]()
                if let latitude = meta.gps.latitude, let longitude = meta.gps.longitude {
                    let value = "\(latitude)(\(meta.gps.latitudeRef!))    \(longitude)(\(meta.gps.longitudeRef!))"
                    let object = self.createObject("GPS", value: String(value))
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray
                    info["title"] = "GPS"
                    self.defalutDataArray.append(info)
                }
            }
        }
    }
    
    //모든 Property를 가져온다. null값도 있다.
    func allDataSetting(pathUrl : NSURL) {
        let meta = GHImageMetaDataHelper.getMetadata(pathUrl)
        if let meta = meta {
            var dataArray = meta.imageproperties.getPropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "imageProperties"
                self.allDataArray.append(info)
            }
            dataArray = meta.tiff.getPropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "TIFF"
                self.allDataArray.append(info)
            }
            dataArray = meta.exif.getPropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "EXIF"
                self.allDataArray.append(info)
            }
            dataArray = meta.gps.getPropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "GPS"
                self.allDataArray.append(info)
            }
        }
    }
    
    //사용할 수 있는 모든 Property를 가져온다. null값이 없다.
    func canUseDataSetting(pathUrl : NSURL) {
        let meta = GHImageMetaDataHelper.getMetadata(pathUrl)
        if let meta = meta {
            var dataArray = meta.imageproperties.canUsePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "imageProperties"
                self.canUseDataArray.append(info)
            }
            dataArray = meta.tiff.canUsePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "TIFF"
                self.canUseDataArray.append(info)
            }
            dataArray = meta.exif.canUsePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "EXIF"
                self.canUseDataArray.append(info)
            }
            dataArray = meta.gps.canUsePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray
                info["title"] = "GPS"
                self.canUseDataArray.append(info)
            }
        }
    }
    
    func resizeCoverImageView() {
        let minValue : CGFloat = 180
        self.contentsViewHeight?.constant = max(64, 300 - (self.tableView?.contentOffset.y)!)
        var rate = min((minValue - self.tableView!.contentOffset.y) / (minValue + self.tableView!.contentOffset.y), 1);
        rate = max(0, rate);
        self.contentsView?.alpha = rate
        self.imageView?.alpha = rate
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.resizeCoverImageView()
    }
    
    @IBAction func backbuttonPressed(sender : AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func segmentedChaged(sender : AnyObject) {
        let index = (self.segment?.selectedSegmentIndex)! as Int
        switch index {
        case 0:
            self.objects = self.defalutDataArray
            print(0)
            break
        case 1:
            self.objects = self.canUseDataArray
            print(1)
            break
        case 2:
            self.objects = self.allDataArray
            print(2)
            break
        default: break
            
        }
        self.tableView?.reloadData()
        self.tableView!.setContentOffset(CGPointMake(0, -20), animated: true)
        
        self.tableView?.alpha = 0.0
        UIView.animateWithDuration(0.3, animations: { 
            self.tableView?.alpha = 1.0
            }) { (isFinish) in
        }
        //        self.tableView?.scrollToRowAtIndexPath(NSIndexPath(forRow:0,inSection:0), atScrollPosition: .Top, animated: true)
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.objects.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let info = self.objects[section]
        let title = info["title"] as! String
        return title
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let info = self.objects[section]
        let object = info["dataArray"] as! [Dictionary<String,AnyObject>]
        return object.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MetaDataTableViewCell", forIndexPath: indexPath) as! MetaDataTableViewCell
        
        let info = self.objects[indexPath.section]
        if let dataArray = info["dataArray"] as? [Dictionary<String,AnyObject>] {
            let object = dataArray[indexPath.row]
            if let name = object["name"] as? String {
                cell.titleLabel?.text = name
            }
            if let value = object["value"] {
                cell.descriptionLabel?.text = "\(value)"
            }
        }
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
