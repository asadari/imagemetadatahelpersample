//
//  GHImageMetaDataHelper.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 6..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO

class Metadata : GHReflectable {
    var imageproperties : GHImageProperties = GHImageProperties()
    var tiff : GHTiff = GHTiff()
    var exif : GHExif = GHExif()
    var gps : GHGps = GHGps()
    init () {
    }
}

class GHImageMetaDataHelper {
    class func getMetadata(url :NSURL) -> Metadata? {
        guard let imageSource = CGImageSourceCreateWithURL(url, nil) else {
            return nil
        }
        guard let imagePropertiesDict = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as NSDictionary? else {
            return nil
        }
        return GHImageMetaDataHelper.getMetadata(imagePropertiesDict)
    }
    
    class func getMetadata(data : NSData) -> Metadata? {
        guard let imageSource = CGImageSourceCreateWithData(data, nil) else {
            return nil
        }
        guard let imagePropertiesDict = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as NSDictionary? else {
            return nil
        }
        return GHImageMetaDataHelper.getMetadata(imagePropertiesDict)
    }
    
    class func getMetadata(imageProperties : NSDictionary) -> Metadata?{
        let metadata = Metadata()
        metadata.imageproperties = GHImageProperties.getElementWithData(imageProperties as? Dictionary<String,AnyObject> )
        if let tiffDict = imageProperties.valueForKey(String(kCGImagePropertyTIFFDictionary)) as? Dictionary<String,AnyObject> {
            metadata.tiff = GHTiff.getElementWithData(tiffDict)
        }
        if let exifDict = imageProperties.valueForKey(String(kCGImagePropertyExifDictionary)) as? Dictionary<String,AnyObject>  {
            metadata.exif = GHExif.getElementWithData(exifDict)
        }
        if let gpsDict = imageProperties.valueForKey(String(kCGImagePropertyGPSDictionary)) as? Dictionary<String,AnyObject>  {
            metadata.gps = GHGps.getElementWithData(gpsDict)
        }
        return metadata
    }
    
    /*
     * 안써봤음
     *
     static func removeExifData(data: NSData) -> NSData? {
     return removeMetadataWithImageProperty(String(kCGImagePropertyExifDictionary), data: data);
     }
     
     static func removeGpsData(data: NSData) -> NSData? {
     return removeMetadataWithImageProperty(String(kCGImagePropertyGPSDictionary), data: data);
     }
     
     static func removeTiffData(data: NSData) -> NSData? {
     return removeMetadataWithImageProperty(String(kCGImagePropertyTIFFDictionary), data: data);
     }
     
     static func removeMetadataWithImageProperty(imagePoperty : String, data : NSData) -> NSData? {
     guard let source = CGImageSourceCreateWithData(data, nil) else {
     return nil
     }
     guard let type = CGImageSourceGetType(source) else {
     return nil
     }
     let count = CGImageSourceGetCount(source)
     let mutableData = NSMutableData(data: data)
     guard let destination = CGImageDestinationCreateWithData(mutableData, type, count, nil) else {
     return nil
     }
     // Check the keys for what you need to remove
     // As per documentation, if you need a key removed, assign it kCFNull
     //        let removeExifProperties: CFDictionary = [String(kCGImagePropertyExifDictionary) : kCFNull, String(kCGImagePropertyOrientation): kCFNull]
     let removeExifProperties: CFDictionary = [imagePoperty : kCFNull]
     
     for i in 0..<count {
     CGImageDestinationAddImageFromSource(destination, source, i, removeExifProperties)
     }
     
     guard CGImageDestinationFinalize(destination) else {
     return nil
     }
     return mutableData;
     }
     */
}



