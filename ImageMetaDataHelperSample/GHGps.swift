//
//  GHGps.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
class GHGps:GHReflectable {
    var isEmpty : Bool = true
    var altitude : Double?
    var altitudeRef : Int?
    var dateStamp : String?
    var destBearing : Double?
    var destBearingRef : String?
    var gpsVersion : [AnyObject]?
    var hPositioningError : Int?
    var imgDirection : Double?
    var imgDirectionRef : String?
    var latitude : Double?
    var latitudeRef : String?
    var longitude : Double?
    var longitudeRef : String?
    var speed : Int?
    var speedRef : String?
    var timeStamp : String?
    
    static func getElementWithData(data : Dictionary<String,AnyObject>?) -> GHGps {
        let gps = GHGps()
        if let data = data {
            gps.isEmpty = false
            gps.altitude = data[String(kCGImagePropertyGPSAltitude)] as? Double
            gps.altitudeRef = data[String(kCGImagePropertyGPSAltitudeRef)] as? Int
            gps.dateStamp = data[String(kCGImagePropertyGPSDateStamp)] as? String
            gps.destBearing = data[String(kCGImagePropertyGPSDestBearing)] as? Double
            gps.destBearingRef = data[String(kCGImagePropertyGPSDestBearingRef)] as? String
            gps.gpsVersion = data[String(kCGImagePropertyGPSVersion)] as? [AnyObject]
            gps.hPositioningError = data[String(kCGImagePropertyGPSHPositioningError)] as? Int
            gps.imgDirection = data[String(kCGImagePropertyGPSImgDirection)] as? Double
            gps.imgDirectionRef = data[String(kCGImagePropertyGPSImgDirectionRef)] as? String
            gps.latitude = data[String(kCGImagePropertyGPSLatitude)] as? Double
            gps.latitudeRef = data[String(kCGImagePropertyGPSLatitudeRef)] as? String
            gps.longitude = data[String(kCGImagePropertyGPSLongitude)] as? Double
            gps.longitudeRef = data[String(kCGImagePropertyGPSLongitudeRef)] as? String
            gps.speed = data[String(kCGImagePropertyGPSSpeed)] as? Int
            gps.speedRef = data[String(kCGImagePropertyGPSSpeedRef)] as? String
            gps.timeStamp = data[String(kCGImagePropertyGPSTimeStamp)] as? String
        }
        return gps
    }
}
